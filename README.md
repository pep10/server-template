## Quick Start

If using vscode you can open the project in a dev container using this plugin: https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-containers

Then either close and reopen the folder in vs code (there will be a pop up asking to open in container) or click the bottom left corner and select open in container

in the container run `npm develop`

#### To setup locally without a container

1. install node 12
  - best accomplished through nvm
2. `npm i` in the project root
3. `npm run develop`


## Using as a Container

```sh
docker build -t server-template .

docker run -it server-template
```

## To Add prettier to You Editor

install this extension in VSCode and update your settings according to the link.

https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode

## To Add .env vars

```sh
touch .env
```

add your variables

```
MY_SECRET=password
```

in your js file

```js
import 'dotenv/config'

const secret = process.env.MY_SECRET
```

## To send a request to example route:

> Before each `npm i` and `npm start`

> If you set an environment variable for the port you will need to adjust the curl script below.

```sh
# send request to basic route
curl --header "Content-Type: application/json" \
  --request POST \
  --data '{"username":"xyz","password":"xyz"}' \
  http://localhost:3000/example

# Should log {"username":"xyz","password":"xyz"} in the server
# and return {"response": "fake response"}
```

```sh
# send request to subrouter
curl localhost:3000/subrouter/example
```
