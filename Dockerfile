FROM node:12-alpine

USER node

WORKDIR /server
RUN mkdir node_modules
COPY --chown=node:node package-lock.json package.json ./
RUN npm ci
COPY --chown=node:node . .
RUN npm run build
EXPOSE 3000
CMD [ "npm", "run", "start"]