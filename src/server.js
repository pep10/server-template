import express, { Router } from 'express';
import cors from 'cors';
import { urlencoded, json } from 'body-parser';

const app = express();
const port = process.env.port || 3000;

// Middleware for routing
// allow localhost queries
app.use(cors());
// parse application/x-www-form-urlencoded
app.use(urlencoded({ extended: false }));
// parse application/json
app.use(json());

// basic route example
app.post('/example', (req, res) => {
    // req is the request object
    // res is the response object

    const { body } = req;

    console.log(body);

    // you can set the status like this
    // res.status(404).send();
    res.send({ response: 'fake response' });
});

// SubRouter example
const subRouter = Router();
subRouter.get('/example', (req, res) => {
    res.send({ onePlusOne: '2' });
});

app.use('/subrouter', subRouter);

app.listen(port, () => console.log(`App listening on ${port}`));
